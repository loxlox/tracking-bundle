<?php

namespace confluence\TrackingBundle\Connector;

use DateTime;
use JsonException;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class Usps extends AbstractConnector
{
    private const USPS_URL = 'http://production.shippingapis.com/ShippingAPI.dll?API=TrackV2%20&XML=%3CTrackFieldRequest%20USERID=%22136CURIO3323%22%3E%20%3CRevision%3E1%3C/Revision%3E%20%3CClientIp%3E127.0.0.1%3C/ClientIp%3E%20%3CSourceId%3EJoe%20Marwell%3C/SourceId%3E%20%3CTrackID%20ID=%22#track#%22%3E%20%3C/TrackID%3E%20%3C/TrackFieldRequest%3E';

    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ClientExceptionInterface
     * @throws JsonException
     */
    public function getResponse(): array
    {
        $url = str_replace(self::TrackDummy, $this->track, self::USPS_URL);

        $response = $this->httpClient->request(
            'GET',
            $url,
            [
                'timeout' => 5,
            ]
        );
        if ($response->getStatusCode() !== 200) {
            return [];
        }
        $content = $response->getContent();
        $xml = simplexml_load_string($content);

        return json_decode(
            json_encode($xml, JSON_THROW_ON_ERROR),
            true,
            512,
            JSON_THROW_ON_ERROR
        );
    }

    public function parseResponse(): void
    {
        $response = $this->response;
        if (!empty($response) && isset($response['TrackInfo']['TrackSummary'])) {
            $package_est_date = !empty($response['TrackInfo']['ExpectedDeliveryDate']) ?
                DateTime::createFromFormat('F j, Y', $response['TrackInfo']['ExpectedDeliveryDate']) : null;
            $status = (string) $response['TrackInfo']['TrackSummary']['Event'];
            $status = str_contains($status, 'Delivered') ? 'Delivered' : $status;

            $this->outputResponse
                ->setStatus($status)
                ->setEstimationDate(
                    $package_est_date instanceof DateTime ?
                        $package_est_date->setTime(0, 0) : null
                );
        }
    }
}
