<?php

namespace confluence\TrackingBundle\Connector;

use DateTime;
use JsonException;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class Ontrac extends AbstractConnector
{
    private const ONTRAC_URL = 'https://www.ontrac.com/services/api/TrackingSummaryByTrackingNumbers/V1/?tracking=#track#';

    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ClientExceptionInterface
     * @throws JsonException
     */
    public function getResponse(): array
    {
        $url = str_replace(self::TrackDummy, $this->track, self::ONTRAC_URL);

        $response = $this->httpClient->request(
            'GET',
            $url,
            [
                'timeout' => 5,
            ]
        );
        if ($response->getStatusCode() !== 200) {
            return [];
        }
        $content = $response->getContent();
        return json_decode($content, true, 512, JSON_THROW_ON_ERROR);
    }

    public function parseResponse(): void
    {
        $response = $this->response;

        if (!empty($response)) {
            $status = $response[0]['StatuscodeDisplayText'];
            $weight = null;
            $package_est_date = $response[0]['ExpectedDeliveryDate'];
            $package_est_date = $package_est_date ?
                DateTime::createFromFormat('m/d/Y', $package_est_date) : null;

            $this->outputResponse
                ->setStatus($status)
                ->setWeight($weight)
                ->setEstimationDate($package_est_date instanceof DateTime ?
                                        $package_est_date->setTime(0, 0) : null);
        }
    }
}
