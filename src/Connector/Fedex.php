<?php

namespace confluence\TrackingBundle\Connector;

use DateTime;
use Exception;
use JsonException;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class Fedex extends AbstractConnector
{
    private const FEDEX_URL = 'https://api.fedex.com/track/v2/shipments';

    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ClientExceptionInterface
     * @throws JsonException
     */
    protected function getResponse(): array
    {
        $data = [
            'appDeviceType' => 'WTRK',
            'appType' => 'WTRK',
            'supportCurrentLocation' => true,
            'trackingInfo' => [
                [
                    'trackNumberInfo' => [
                        'trackingCarrier' => '',
                        'trackingNumber' => $this->track,
                        'trackingQualifier' => '',
                    ]
                ]
            ],
            'uniqueKey' => ''
        ];
        $response = $this->httpClient->request(
            'POST',
            self::FEDEX_URL,
            [
                'headers' => [
                    'Authorization' => 'Bearer l7b8ada987a4544ff7a839c8e1f6548eea',
                    'User-Agent' => 'Mozilla/5.0 (X11; Linux x86_64; rv:97.0) Gecko/20100101 Firefox/97.0',
                    'Accept' => 'application/json',
                    'Referer' => 'https://www.fedex.com/en-us/home.html',
                    'X-Requested-With' => 'XMLHttpRequest',
                    'Content-Type' => 'application/json',
                    'Accept-Language' => 'en-EN,en;q=0.8,en-US;q=0.5,en;q=0.3',
                ],
                'body' => json_encode($data, JSON_THROW_ON_ERROR)
            ]
        );
        if ($response->getStatusCode() !== 200) {
            return [];
        }
        $content = $response->getContent();

        return json_decode($content, true, 512, JSON_THROW_ON_ERROR);
    }

    /**
     * @throws Exception
     */
    public function parseResponse(): void
    {
        $response = $this->response;
        if (empty($response['output']['packages'][0])) {
            $this->outputResponse
                ->setStatus(null)
                ->setWeight(null)
                ->setEstimationDate(null);
            return;
        }
        $jsonData = $response['output']['packages'][0];
        $status = $jsonData['keyStatus'];
        $weight = $jsonData['pkgLbsWgt'];
        $estDate = ($status === 'Delivered')
            ? DateTime::createFromFormat('Y-m-d\TH:i:sP', $jsonData['actDeliveryDt'])
            : DateTime::createFromFormat('Y-m-d\TH:i:sP', $jsonData['estDeliveryDt']);

        $this->outputResponse
            ->setStatus($status)
            ->setWeight($weight)
            ->setEstimationDate(($estDate instanceof DateTime) ? $estDate : null);
    }
}
