<?php

namespace confluence\TrackingBundle\Connector;

use confluence\TrackingBundle\Dto\ResponseDto;
use Symfony\Contracts\HttpClient\HttpClientInterface;

abstract class AbstractConnector
{
    public const TrackDummy = '#track#';
    protected ResponseDto $outputResponse;
    protected array $response = [];
    protected string $track;

    public function __construct(string $track, protected HttpClientInterface $httpClient)
    {
        $this->track = $track;
        $this->outputResponse = new ResponseDto(null, null, null, null);
    }

    public function getShippingResponse(): ResponseDto
    {
        $this->response = $this->getResponse();
        $this->parseResponse();

        return $this->outputResponse;
    }

    abstract protected function getResponse(): array;

    abstract protected function parseResponse(): void;
}
