<?php

namespace confluence\TrackingBundle\Connector;

use DateTime;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class Dhl extends AbstractConnector
{
    public const DHL_URL = 'http://www.dhl.com/shipmentTracking?AWB=#track#&countryCode=g0&languageCode=en';

    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ClientExceptionInterface
     */
    protected function getResponse(): array
    {
        $url = str_replace(self::TrackDummy, $this->track, self::DHL_URL);

        $response = $this->httpClient->request(
            'GET',
            $url
        );

        $content = $response->getContent();

        return json_decode($content, true, 512, JSON_THROW_ON_ERROR);
    }

    protected function parseResponse(): void
    {
        $response = $this->response;

        $status = !empty($response['results'][0]['delivery']['status']) ?
            ucfirst($response['results'][0]['delivery']['status']) :
            null;

        if (!empty($response['results'][0]['edd']['product'])) {
            if (!str_contains($response['results'][0]['edd']['product'], 'today')) {
                $package_est_date = new DateTime('NOW');
            } else {
                $package_est_date = null;
            }
        } else {
            if (!empty($response['results'][0]['signature']['description']) &&
                preg_match('/^(\w+),\s([\w\s,]+)\sat\s(\d+:\d+)/', $response['results'][0]['signature']['description'], $results)) {
                $package_est_date = DateTime::createFromFormat('F d, Y', trim($results[2]));
            } else {
                $package_est_date = null;
            }
        }

        $this->outputResponse
            ->setStatus($status)
            ->setEstimationDate($package_est_date instanceof DateTime
                                    ? $package_est_date->setTime(0, 0) : null);
    }
}
