<?php

namespace confluence\TrackingBundle\Connector;

use DateTime;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class Glsus extends AbstractConnector
{
    private const GLSUS_URL = 'https://www.gls-us.com/Tracking';
//    private const FILE_COOKIE = 'tmp/glsus_cookie.txt';

    /**
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     * @return array
     */
    public function getResponse(): array
    {
        $response = $this->httpClient->request(
            'POST',
            self::GLSUS_URL,
            [
                'body' => 'TrackingNumber='.$this->track,
            ]
        );

        if ($response->getStatusCode() !== 200) {
            return [];
        }

        return ['data' => $response->getContent()];
    }

    public function parseResponse(): void
    {
        $response = $this->response['data'];
        preg_match(
            '/Delivery Status:<\/b><\/td>\s+<td\sstyle=\"width:\s80px;\">\s+<span>(.*?)<\/span>/',
            $response,
            $status
        );
        preg_match('/Delivery Date:<\/b><\/td>\s+<td>\s+<span>(.*?)<\/span>/', $response, $date);
        if (isset($date[1])) {
            $date = DateTime::createFromFormat('m/d/Y', $date[1]);
        }

        $this->outputResponse
            ->setStatus(isset($status[1]) ? ucfirst(strtolower($status[1])) : null)
            ->setWeight(null)
            ->setEstimationDate($date ?: null);
    }
}
