<?php

namespace confluence\TrackingBundle\Connector;

use DateTime;
use JsonException;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class Ups extends AbstractConnector
{
    // const UPS_URL = "http://wwwapps.ups.com/etracking/tracking.cgi?InquiryNumber1=#track#&tracknums_displayed=5&TypeOfInquiryNumber=T&HTMLVersion=4.0";
    // const UPS_URL = "https://wwwapps.ups.com/WebTracking/track?track=yes&trackNums=#track#";
    private const UPS_URL = 'https://www.ups.com/track/api/Track/GetStatus';

    /**
     * @throws JsonException
     * @throws TransportExceptionInterface
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @return array
     */
    public function getResponse(): array
    {
        $data = json_encode([
            'Locale' => 'en_US',
            'Requester' => 'wt',
            'TrackingNumber' => [
                $this->track,
            ],
        ], JSON_THROW_ON_ERROR);

        $response = $this->httpClient->request(
            'POST',
            self::UPS_URL,
            [
                'headers' => [
                    'User-Agent' => 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko)'
                        .'Chrome/28.0.1500.72 Safari/537.36',
                    'Content-Type' => 'application/json; charset=utf-8',
                    'Accept' => 'application/json, text/javascript, */*; q=0.01',
                    'X-XSRF-TOKEN' => 'CfDJ8Jcj9GhlwkdBikuRYzfhrpIOFHy50PJCv1NDFPHLoXdO2cGcNccQ1hq28loKQjrSAtxpto5yCzne4jK-A2dMo4qejR3l5PsJCt_4T6SXU9ximnmMPXJoHbsg8IigALQ1A_zbRFAm32CD7Kw-F5VsPlw',
                    'Cookie' => 'X-CSRF-TOKEN=CfDJ8Jcj9GhlwkdBikuRYzfhrpKXjfaDqxZP0ecjNGWF0JE3KBJrMbvEvNmSHqlSUBTxyZ8-hgJaN0wh2QalwgaSKlPF3FIDerRdnLXxMa5-3ZYJ0zf5qz3wMgqOaxgiPsuG-cdFBhwKEcaAit9-q-FMh60;',
                ],
                'body' => $data
            ]
        );
        if ($response->getStatusCode() !== 200) {
            return [];
        }
        $content = $response->getContent();

        return json_decode($content, true, 512, JSON_THROW_ON_ERROR);
    }

    public function parseResponse(): void
    {
        $response = $this->response;

        if (isset($response['statusCode'], $response['statusText'], $response['trackDetails']) &&
            $response['statusCode'] === '200' && $response['statusText'] === 'Successful' &&
            $response['trackDetails'][0]['errorCode'] === null) {
            $packageEstDate = null;
            $trackDetails = $response['trackDetails'][0];
            $status = $trackDetails['packageStatus'];
            $weight = $trackDetails['additionalInformation']['weight'];
            if (!empty($trackDetails['deliveredDate'])) {
                $packageEstDate = DateTime::createFromFormat(
                    'm/d/Y g:i A',
                    $trackDetails['deliveredDate'].' '.
                                                             $trackDetails['deliveredTime']
                );
            } elseif (!empty($trackDetails['scheduledDeliveryDate'])) {
                $packageEstDate = DateTime::createFromFormat(
                    'm/d/Y',
                    $trackDetails['scheduledDeliveryDate']
                );
            }

            $this->outputResponse
                ->setStatus($status)
                ->setWeight($weight)
                ->setEstimationDate(($packageEstDate) ?: null);
        }
    }
}
