<?php

namespace confluence\TrackingBundle\Connector;

use DateTime;
use JsonException;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class Lasership extends AbstractConnector
{
    private const LASERSHIP_URL = 'http://www.lasership.com/track/#track#/json';

    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ClientExceptionInterface
     * @throws JsonException
     */
    public function getResponse(): array
    {
        $url = str_replace(self::TrackDummy, $this->track, self::LASERSHIP_URL);
        $response = $this->httpClient->request(
            'GET',
            $url
        );
        if ($response->getStatusCode() !== 200) {
            return [];
        }
        $content = $response->getContent();

        return json_decode($content, true, 512, JSON_THROW_ON_ERROR);
    }

    public function parseResponse(): void
    {
        $response = $this->response;

        $status = null;
        if (!empty($response['Events']) && $event = array_shift($response['Events'])) {
            $status = !empty($event['EventLabel']) ? $event['EventLabel'] : null;
        }

        $package_est_date = !empty($response['EstimatedDeliveryDate']) ?
            DateTime::createFromFormat('Y-m-d', $response['EstimatedDeliveryDate']) :
            null;

        $weight = !empty($response['Pieces'][0]['Weight']) ? $response['Pieces'][0]['Weight'] : null;

        $this->outputResponse
            ->setStatus($status)
            ->setWeight($weight)
            ->setEstimationDate($package_est_date instanceof DateTime ?
                                    $package_est_date->setTime(0, 0) : null);
    }
}
