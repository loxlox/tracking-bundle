<?php

namespace confluence\TrackingBundle\Connector;

use DateTime;
use JsonException;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class Deliverit extends AbstractConnector
{
    private const DHL_URL = 'https://api.concise.io/public/v3/di/shipment/#track#';

    /**
     * @throws JsonException
     * @throws TransportExceptionInterface
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @return array
     */
    public function getResponse(): array
    {
        $url = str_replace(self::TrackDummy, $this->track, self::DHL_URL);

        $response = $this->httpClient->request(
            'GET',
            $url
        );
        if ($response->getStatusCode() !== 200) {
            return [];
        }

        return json_decode($response->getContent(), true, 512, JSON_THROW_ON_ERROR);
    }

    public function parseResponse(): void
    {
        $response = $this->response;

        $status = (!empty($response['data']['current_status'])) ? $response['data']['current_status'] : null;
        $weight = (!empty($response['data']['weight'])) ? $response['data']['weight'] : null;

        if ($status === 'Delivered') {
            $estDate = (!empty($response['data']['dates']['deliver_time'])) ?
                DateTime::createFromFormat('U', $response['data']['dates']['deliver_time']) : null;
        } else {
            $estDate = (!empty($response['data']['dates']['due_time'])) ?
                DateTime::createFromFormat('U', $response['data']['dates']['due_time']) : null;
        }

        $this->outputResponse
            ->setStatus($status)
            ->setWeight($weight)
            ->setEstimationDate($estDate instanceof DateTime
                                    ? $estDate->setTime(0, 0) : null);
    }
}
