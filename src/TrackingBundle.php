<?php

namespace confluence\TrackingBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class TrackingBundle extends Bundle
{
    public function getPath(): string
    {
        return dirname(__DIR__);
    }
}
