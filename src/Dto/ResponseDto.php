<?php

namespace confluence\TrackingBundle\Dto;

use DateTime;

class ResponseDto
{
    private ?string $message = null;
    private ?string $status = null;
    private ?string $weight = null;
    private ?DateTime $estimationDate = null;

    /**
     * @param string|null   $message
     * @param string|null   $status
     * @param string|null   $weight
     * @param DateTime|null $estimationDate
     */
    public function __construct(
        ?string $message,
        ?string $status,
        ?string $weight,
        ?DateTime $estimationDate
    ) {
        $this->message = $message;
        $this->status = $status;
        $this->weight = $weight;
        $this->estimationDate = $estimationDate;
    }


    public function setMessage(?string $message): ResponseDto
    {
        $this->message = $message;

        return $this;
    }

    public function getMessage(): ?string
    {
        return $this->message;
    }

    public function setStatus(?string $status): ResponseDto
    {
        $this->status = $status;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setWeight(?string $weight): ResponseDto
    {
        $this->weight = $weight;

        return $this;
    }

    public function getWeight(): ?string
    {
        return $this->weight;
    }

    public function setEstimationDate(?DateTime $estimationDate): ResponseDto
    {
        $this->estimationDate = $estimationDate;

        return $this;
    }

    public function getEstimationDate(): ?DateTime
    {
        return $this->estimationDate;
    }
}
