<?php

namespace confluence\TrackingBundle\Service;

use confluence\TrackingBundle\Connector\AbstractConnector;
use confluence\TrackingBundle\Dto\ResponseDto;
use RuntimeException;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class ShippingSystemChecker
{
    private ?string $track;
    private ?string $shippingName;

    public function __construct(private HttpClientInterface $httpClient)
    {
    }

    /**
     * @return ResponseDto
     */
    private function getResponse(): ResponseDto
    {
        return $this->getShippingSystemConnector()->getShippingResponse();
    }

    /**
     * @return AbstractConnector
     */
    private function getShippingSystemConnector(): AbstractConnector
    {
        $className = 'confluence\\TrackingBundle\\Connector\\'.ucfirst(strtolower($this->shippingName));
        if (class_exists($className)) {
            return new $className($this->track, $this->httpClient);
        }
        throw new RuntimeException(sprintf(
            'Class connector not found: %s',
            ucfirst(strtolower($this->shippingName))
        ));
    }

    public function trackShipping(string $trackNumber, string $shippingNme): ResponseDto
    {
        $this->track = $trackNumber;
        $this->shippingName = strtoupper($shippingNme);
        return $this->getResponse();
    }

    public function autoUpdateStatus(string $status): bool
    {
        if (!empty($status)) {
            switch ($this->shippingName) {
                case 'FEDEX':
                    if (!in_array($status, [
                        'Shipment information sent to FedEx',
                        'Shipment cancelled by sender',
                    ], true)) {
                        return true;
                    }
                    break;
                case 'USPS':
                    if (!in_array($status, [
                        'Pre-Shipment Info Sent to USPS, USPS Awaiting Item',
                        'Shipping Label Created, USPS Awaiting Item',
                    ], true)) {
                        return true;
                    }
                    break;
                case 'UPS':
                    if (!in_array($status, [
                        'Order Processed: Ready for UPS',
                        'Shipment Ready for UPS',
                    ], true)) {
                        return true;
                    }
                    break;
                default:
            }
        }

        return false;
    }

    public function checkReturn(?string $status): bool
    {
        foreach (['return', 'intercepted'] as $sign) {
            if (is_numeric(stripos($status, $sign))) {
                return true;
            }
        }

        return false;
    }
}
