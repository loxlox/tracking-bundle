<?php

namespace confluence\TrackingBundle\Tests\Service;

use confluence\TrackingBundle\Service\ShippingSystemChecker;
use PHPUnit\Framework\TestCase;
use RuntimeException;
use Symfony\Component\HttpClient\MockHttpClient;

class ShippingSystemCheckerTest extends TestCase
{
    public function testTrackShippingWithIncorrectShippingId(): void
    {
        $checker = new ShippingSystemChecker(new MockHttpClient());

        $this->expectException(RuntimeException::class);
        $this->expectExceptionMessage('Class connector not found');
        $checker->trackShipping('1111', 'Incorrect');
    }

    public function returnStatusesProvider(): array
    {
        return [
            ['Return to sender', true],
            ['Package intercepted', true],
            ['', false],
            [null, false],
            ['Delivered', false],
        ];
    }

    /**
     * @dataProvider returnStatusesProvider
     */
    public function testCheckReturn($status, $expected): void
    {
        $checker = new ShippingSystemChecker(new MockHttpClient());
        $this->assertSame($expected, $checker->checkReturn($status));
    }
}
