<?php

namespace confluence\TrackingBundle\Tests\Connector;

use confluence\TrackingBundle\Service\ShippingSystemChecker;
use DateTime;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpClient\MockHttpClient;
use Symfony\Component\HttpClient\Response\MockResponse;

class UpsTest extends TestCase
{
    public function testTrackingUps(): void
    {
        $responses = [
            new MockResponse(json_encode([
                'statusCode' => '200',
                'statusText' => 'Successful',
                'trackDetails' => [
                    [
                        'errorCode' => null,
                        'packageStatus' => 'In transit',
                        'additionalInformation' => [
                            'weight' => '2.00'
                        ],
                        'deliveredDate' => '01/02/2021',
                        'deliveredTime' => '11:56 A.M.'
                    ]
                ],
            ], JSON_THROW_ON_ERROR), ['success tracking delivered']),
            new MockResponse(json_encode([
                'statusCode' => '200',
                'statusText' => 'Successful',
                'trackDetails' => [
                    [
                        'errorCode' => null,
                        'packageStatus' => null,
                        'additionalInformation' => [
                            'weight' => null
                        ],
                        'scheduledDeliveryDate' => '01/02/2021',
                    ]
                ],
            ], JSON_THROW_ON_ERROR), ['success tracking in transit']),
            new MockResponse(json_encode([], JSON_THROW_ON_ERROR), ['empty result'])
        ];
        $client = new MockHttpClient($responses);
        $checker = new ShippingSystemChecker($client);

        $result = $checker->trackShipping('1', 'Ups');
        $this->assertEquals('In transit', $result->getStatus());
        $this->assertEquals('2.00', $result->getWeight());
        $this->assertInstanceOf(DateTime::class, $result->getEstimationDate());
        $this->assertEquals('02-01-2021', $result->getEstimationDate()->format('d-m-Y'));

        $result = $checker->trackShipping('1', 'Ups');
        $this->assertNull($result->getStatus());
        $this->assertNull($result->getWeight());
        $this->assertInstanceOf(DateTime::class, $result->getEstimationDate());
        $this->assertEquals('02-01-2021', $result->getEstimationDate()->format('d-m-Y'));

        $result = $checker->trackShipping('1', 'Ups');
        $this->assertNull($result->getStatus());
        $this->assertNull($result->getMessage());
        $this->assertNull($result->getWeight());
        $this->assertNull($result->getEstimationDate());
    }
}
