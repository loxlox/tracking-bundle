<?php

namespace confluence\TrackingBundle\Tests\Connector;

use confluence\TrackingBundle\Service\ShippingSystemChecker;
use DateTime;
use JsonException;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpClient\MockHttpClient;
use Symfony\Component\HttpClient\Response\MockResponse;

class DeliveritTest extends TestCase
{
    /**
     * @throws JsonException
     */
    public function testTrackingDeliverit(): void
    {
        $responses = [
            new MockResponse(
                json_encode([
                    'data' => [
                        'current_status' => 'In transit',
                        'weight' => '1',
                        'dates' => [
                            'due_time' => '1609612049'
                        ],
                    ]
                ], JSON_THROW_ON_ERROR),
                ['success tracking']
            ),
            new MockResponse(
                json_encode([
                    'data' => [
                        'current_status' => 'Delivered',
                        'weight' => '1',
                        'dates' => [
                            'deliver_time' => '1609612049'
                        ],
                    ]
                ], JSON_THROW_ON_ERROR),
                ['delivered']
            ),
            new MockResponse(
                json_encode([], JSON_THROW_ON_ERROR),
                ['empty result']
            )
        ];
        $client = new MockHttpClient($responses);
        $checker = new ShippingSystemChecker($client);

        $result = $checker->trackShipping('1', 'DeliverIt');
        $this->assertEquals('In transit', $result->getStatus());
        $this->assertInstanceOf(DateTime::class, $result->getEstimationDate());
        $this->assertEquals('02-01-2021', $result->getEstimationDate()->format('d-m-Y'));
        $this->assertEquals(1, $result->getWeight());

        $result = $checker->trackShipping('1', 'DeliverIt');
        $this->assertEquals('Delivered', $result->getStatus());
        $this->assertInstanceOf(DateTime::class, $result->getEstimationDate());
        $this->assertEquals('02-01-2021', $result->getEstimationDate()->format('d-m-Y'));
        $this->assertEquals(1, $result->getWeight());

        $result = $checker->trackShipping('1', 'DeliverIt');
        $this->assertNull($result->getStatus());
        $this->assertNull($result->getMessage());
        $this->assertNull($result->getEstimationDate());
        $this->assertNull($result->getWeight());
    }
}
