<?php

namespace confluence\TrackingBundle\Tests\Connector;

use confluence\TrackingBundle\Service\ShippingSystemChecker;
use DateTime;
use JsonException;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpClient\MockHttpClient;
use Symfony\Component\HttpClient\Response\MockResponse;

class GlsusTest extends TestCase
{
    /**
     * @throws JsonException
     */
    public function testTrackingGlsus(): void
    {
        $responses = [
            new MockResponse(
                '<td><b>Delivery Status:</b></td> <td style="width: 80px;"> <span>In transit</span><td><b>Delivery Date:</b></td> <td> <span>1/2/2021</span>',
                ['success tracking']
            ),
            new MockResponse(
                json_encode([], JSON_THROW_ON_ERROR),
                ['empty result']
            )
        ];
        $client = new MockHttpClient($responses);
        $checker = new ShippingSystemChecker($client);

        $result = $checker->trackShipping('1', 'Glsus');
        $this->assertEquals('In transit', $result->getStatus());
        $this->assertInstanceOf(DateTime::class, $result->getEstimationDate());
        $this->assertEquals('02-01-2021', $result->getEstimationDate()->format('d-m-Y'));

        $result = $checker->trackShipping('1', 'Glsus');
        $this->assertNull($result->getStatus());
        $this->assertNull($result->getMessage());
        $this->assertNull($result->getEstimationDate());
    }
}
