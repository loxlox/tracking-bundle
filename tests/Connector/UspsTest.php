<?php

namespace confluence\TrackingBundle\Tests\Connector;

use confluence\TrackingBundle\Service\ShippingSystemChecker;
use DateTime;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpClient\MockHttpClient;
use Symfony\Component\HttpClient\Response\MockResponse;

class UspsTest extends TestCase
{
    public function testTrackingUsps(): void
    {
        $responses = [
            new MockResponse(<<<XML
<?xml version="1.0" encoding="UTF-8"?>
<root>
   <TrackInfo>
      <TrackSummary>
         <Event>In transit</Event>
      </TrackSummary>
      <ExpectedDeliveryDate>January 02, 2021</ExpectedDeliveryDate>
   </TrackInfo>
</root>
XML, ['success tracking in transit']),
            new MockResponse(<<<XML
<?xml version="1.0" encoding="UTF-8"?>
<root />
XML, ['empty result']),
        ];
        $client = new MockHttpClient($responses);
        $checker = new ShippingSystemChecker($client);

        $result = $checker->trackShipping('1', 'usps');
        $this->assertEquals('In transit', $result->getStatus());
        $this->assertInstanceOf(DateTime::class, $result->getEstimationDate());
        $this->assertEquals('02-01-2021', $result->getEstimationDate()->format('d-m-Y'));

        $result = $checker->trackShipping('1', 'usps');
        $this->assertNull($result->getStatus());
        $this->assertNull($result->getMessage());
        $this->assertNull($result->getEstimationDate());
    }
}
