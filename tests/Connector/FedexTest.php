<?php

namespace confluence\TrackingBundle\Tests\Connector;

use confluence\TrackingBundle\Service\ShippingSystemChecker;
use DateTime;
use JsonException;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpClient\MockHttpClient;
use Symfony\Component\HttpClient\Response\MockResponse;

class FedexTest extends TestCase
{
    /**
     * @throws JsonException
     */
    public function testTrackingFedex(): void
    {
        $responses = [
            new MockResponse('', ['http_code' => 500, 'failed tracking']),
            new MockResponse(json_encode([
                'output' => [
                    'packages' => [
                        [
                            'keyStatus' => 'In transit',
                            'pkgLbsWgt' => '2.5',
                            'estDeliveryDt' => '2021-01-02T00:00:00-06:00',
                        ]
                    ]
                ]
            ], JSON_THROW_ON_ERROR), ['success tracking in transit']),
            new MockResponse(json_encode([
                'output' => [
                    'packages' => [
                        [
                            'keyStatus' => 'Delivered',
                            'pkgLbsWgt' => '2.5',
                            'actDeliveryDt' => '2021-01-02T00:00:00-06:00',
                        ]
                    ]
                ],
            ], JSON_THROW_ON_ERROR), ['success tracking delivered']),
        ];
        $client = new MockHttpClient($responses);
        $checker = new ShippingSystemChecker($client);

        $result = $checker->trackShipping('1', 'Fedex');
        $this->assertNull($result->getStatus());
        $this->assertNull($result->getWeight());
        $this->assertNull($result->getEstimationDate());

        $result = $checker->trackShipping('1', 'Fedex');
        $this->assertEquals('In transit', $result->getStatus());
        $this->assertEquals('2.5', $result->getWeight());
        $this->assertInstanceOf(DateTime::class, $result->getEstimationDate());
        $this->assertEquals('02-01-2021', $result->getEstimationDate()->format('d-m-Y'));

        $result = $checker->trackShipping('1', 'Fedex');
        $this->assertEquals('Delivered', $result->getStatus());
        $this->assertInstanceOf(DateTime::class, $result->getEstimationDate());
        $this->assertEquals('02-01-2021', $result->getEstimationDate()->format('d-m-Y'));
    }
}
