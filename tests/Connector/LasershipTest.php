<?php

namespace confluence\TrackingBundle\Tests\Connector;

use confluence\TrackingBundle\Service\ShippingSystemChecker;
use DateTime;
use JsonException;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpClient\MockHttpClient;
use Symfony\Component\HttpClient\Response\MockResponse;

class LasershipTest extends TestCase
{
    /**
     * @throws JsonException
     */
    public function testTrackingLaserShip(): void
    {
        $responses = [
            new MockResponse(json_encode([
                'Events' => [
                    [
                        'EventLabel' => 'In transit',
                    ],
                    [
                        'EventLabel' => 'test status2',
                    ]
                ],
                'EstimatedDeliveryDate' => '2021-01-02',
                'Pieces' => [
                    [
                        'Weight' => 21,
                        'WeightUnit' => 'lbs'
                    ]
                ]
            ], JSON_THROW_ON_ERROR), ['success tracking']),
            new MockResponse(json_encode([], JSON_THROW_ON_ERROR), ['empty result'])
        ];
        $client = new MockHttpClient($responses);
        $checker = new ShippingSystemChecker($client);

        $result = $checker->trackShipping('1', 'LaserShip');
        $this->assertEquals('In transit', $result->getStatus());
        $this->assertEquals('21', $result->getWeight());
        $this->assertInstanceOf(DateTime::class, $result->getEstimationDate());
        $this->assertEquals('02-01-2021', $result->getEstimationDate()->format('d-m-Y'));

        $result = $checker->trackShipping('1', 'LaserShip');
        $this->assertNull($result->getStatus());
        $this->assertNull($result->getMessage());
        $this->assertNull($result->getWeight());
        $this->assertNull($result->getEstimationDate());
    }
}
