<?php

namespace confluence\TrackingBundle\Tests\Connector;

use confluence\TrackingBundle\Service\ShippingSystemChecker;
use DateTime;
use JsonException;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpClient\MockHttpClient;
use Symfony\Component\HttpClient\Response\MockResponse;

class OntracTest extends TestCase
{
    /**
     * @throws JsonException
     */
    public function testTrackingOntrack(): void
    {
        $responses = [
            new MockResponse(json_encode([
                [
                    'StatuscodeDisplayText' => 'In transit',
                    'ExpectedDeliveryDate' => '01/02/2021'
                ]
            ], JSON_THROW_ON_ERROR), ['success tracking']),
            new MockResponse(json_encode([], JSON_THROW_ON_ERROR), ['empty result'])
        ];
        $client = new MockHttpClient($responses);
        $checker = new ShippingSystemChecker($client);

        $result = $checker->trackShipping('1', 'Ontrac');
        $this->assertEquals('In transit', $result->getStatus());
        $this->assertNull($result->getWeight());
        $this->assertNull($result->getMessage());
        $this->assertInstanceOf(DateTime::class, $result->getEstimationDate());

        $result = $checker->trackShipping('1', 'Ontrac');
        $this->assertNull($result->getStatus());
        $this->assertNull($result->getMessage());
        $this->assertNull($result->getWeight());
        $this->assertNull($result->getEstimationDate());
    }
}
