<?php

namespace confluence\TrackingBundle\Tests\Connector;

use confluence\TrackingBundle\Service\ShippingSystemChecker;
use DateTime;
use JsonException;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpClient\MockHttpClient;
use Symfony\Component\HttpClient\Response\MockResponse;

class DhlTest extends TestCase
{
    /**
     * @throws JsonException
     */
    public function testTrackingDhl(): void
    {
        $responses = [
            new MockResponse(
                json_encode([
                    'results' => [
                        [
                            'delivery' => [
                                'status' => 'In transit'
                            ],
                            'signature' => [
                                'description' => 'sss, January 02, 2021 at 33:33'
                            ]
                        ]
                    ]
                ], JSON_THROW_ON_ERROR),
                ['success tracking']
            ),
            new MockResponse(
                json_encode([
                    'results' => [
                        [
                            'edd' => [
                                'product' => '1'
                            ]
                        ]
                    ]
                ], JSON_THROW_ON_ERROR),
                ['delivered']
            ),
            new MockResponse(
                json_encode([], JSON_THROW_ON_ERROR),
                ['empty result']
            )
        ];
        $client = new MockHttpClient($responses);
        $checker = new ShippingSystemChecker($client);

        $result = $checker->trackShipping('1', 'Dhl');
        $this->assertEquals('In transit', $result->getStatus());
        $this->assertInstanceOf(DateTime::class, $result->getEstimationDate());
        $this->assertEquals('02-01-2021', $result->getEstimationDate()->format('d-m-Y'));

        $result = $checker->trackShipping('1', 'Dhl');
        $this->assertNull($result->getStatus());
        $this->assertInstanceOf(DateTime::class, $result->getEstimationDate());

        $result = $checker->trackShipping('1', 'Dhl');
        $this->assertNull($result->getStatus());
        $this->assertNull($result->getMessage());
        $this->assertNull($result->getEstimationDate());
    }
}
